<?php /**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link href='http://fonts.googleapis.com/css?family=Lobster|Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="../wp-content/themes/Accelerate_Progress/js/output.min.js"></script>
    <link href='../wp-content/themes/Accelerate_Progress/css/style.min.css' rel='stylesheet' type='text/css' >
    
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<div id="content" class="site-content">
