$(document).ready(function() {
    
// Client Logo/Screen/Content Section
    
    $(".client-logo").hover(
      function() {
        $(this).toggleClass("hide-logo"); 
        $(this).click(
        function() {
            $(this).siblings(".client-content").toggleClass("hide-screen");
        });

    });

  

// End Client Logo/Screen/Content Section 
    
// Fork/Code Callout 
    
    $(window).scroll(function() {
    if ($(this).scrollTop() > 800){  
        $('#fork-me').addClass("sticky");
        $("#close-box").click(function() {
            $('#fork-me').addClass("hide");
        });
      }
      else if ($(this).scrollTop() === $(document).height()) {
        $('#fork-me').removeClass("sticky");
        $('#fork-me').removeClass("hide");
      }
      else {
        $('#fork-me').removeClass("sticky");
        $('#fork-me').removeClass("hide");
      };
    });
    

    
// End Fork/Code Callout 
    
// Contact me/Social Icons/Footer Section
    
    $(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() + "100" > $(document).height()) {
            $('#contact').addClass("contact-show");
        }
    });
    
    $(window).scroll(function() {
    if ($(this).scrollTop() < 800) {  
            $('#contact').removeClass("contact-show");
            $('contact-box').removeClass("contact-box-show");
        } 
    });

    $(".contact-text").click(function() {
        $(this).slideToggle("slow", function() {
            $("#social-icons").slideToggle("slow", function() { 
          });
        });
    });
    
    $(".widgettitle").click(function() {
      $("#social-icons").slideToggle("slow", function() {
           $(".contact-text").slideToggle("slow", function() {
          });
      });
    });
    
// End Contact me/Social Icons/Footer Section
   
$('a').each(function() {
   var a = new RegExp('/' + window.location.host + '/');
   if(!a.test(this.href)) {
       $(this).click(function(event) {
           event.preventDefault();
           event.stopPropagation();
           window.open(this.href, '_blank');
       });
   }
});
    
});
    

