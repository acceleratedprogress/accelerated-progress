<?php if ( is_active_sidebar( 'saintly-contact-social' ) ) : ?>
	<div class="saintly-contact-social" role="complementary">
		<?php dynamic_sidebar( 'saintly-contact-social' ); ?>
	</div><!-- .widget-area -->
<?php endif; ?>