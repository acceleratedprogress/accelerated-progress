<?php

/* 
    Template Name: Accelerate Progress Home
 */

get_header();
?>
<!-- Start Content Area -->
<div class="content-area">
    <!-- Hero Area -->
    <main id="hero" class="site-main" role="main">            
        <div class="hero-img" style="background-image: url(<?php $image = the_field('hero_image'); ?>)">
            <h1><?php single_post_title(); ?></h1>
            <h2>
                <?php 
                    if ( have_posts() ) : while( have_posts() ) : the_post();
                        the_content();
                    endwhile; 
                endif; ?>
            </h2>
        </div>
    </main>
    <!-- End Hero Area -->
    <!-- About Me Area -->
    <div id="about-me">
       <h2><?php the_field('welcome_title'); ?></h2>

       <p><?php the_field('about_me'); ?></p>
    </div>

    <div id="about-me-banner" style="background-image: url('<?php the_field("banner"); ?>');"></div>

    <div id="main-banner">
        <h2><?php the_field('main_title'); ?></h2>
        <p><?php the_field('main_content'); ?></p>
    </div>
    <!-- End About Me Area -->
    <!-- Client Custom Post Type Area -->
    <div id="clients-wrapper">
        <h2><?php the_field('clients_wrapper_title'); ?></h2>
        <p><?php the_field('clients_wrapper_content'); ?></p>
        <div id="clients"> 
            <?php
                $args = array( 'post_type' => 'SaintlyClients', 'posts_per_page' => 16 );
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post();
                 echo '<div class="client-box">';
                    echo '<div class="client-logo">';
                        the_post_thumbnail("full");
                    echo '</div>';
                    echo '<div class="client-screen">';
                        echo '<img src="';
                             the_field('screen_shot');
                        echo '">';
                    echo '</div>';
                    echo '<div class="client-content">';
                        echo '<h1>';
                                the_title();    
                        echo '</h1>';
                        echo '<p>';
                            the_content();
                        echo '</p>';
                    echo '</div>';
                echo '</div>';
                endwhile; 
                wp_reset_postdata();
            ?>
        </div> 
    </div>
    <!-- End Client Custom Post Type Area -->
    <!-- Fork Me Area -->
    <div id="fork-me">
        <img id="close-box" src="../wp-content/themes/Accelerate_Progress/img/closebox.png">
        <h2><?php the_field('fork_me');?></h2>
        <a href="<?php the_field('fork_me_link');?>" target="_blank"><img src="../wp-content/themes/Accelerate_Progress/img/bitbucket.png"/></a>
    </div>
    <!-- Start Contact Area -->
    <div id="contact">
        <div class="contact-content">
            <div class="contact-text">
                 <h2><?php the_field('say_hello');?></h2>
            </div>
            <div id="social-icons">
                <?php get_template_part('saintly-contact-social'); ?>
                <?php the_field('contact_me');?>
            </div>

        </div>
    </div>
    <!-- End Contact Area --> 
</div>
<!-- Content Area -->
<?php get_footer(); ?>
