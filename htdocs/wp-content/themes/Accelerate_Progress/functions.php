<?php
/**
 * Saintly functions and definitions
 */

/**
 * JavaScript Detection.
 */
function saintly_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'saintly_javascript_detection', 0 );




// This theme uses wp_nav_menu() in two locations.
register_nav_menus( array(
    'primary' => __( 'Primary Menu',      'saintly' ),
    'social'  => __( 'Social Links Menu', 'saintly' ),
) );


/**
 * Enqueue scripts and styles.
 */

function saintly_scripts() {
	// Load our main stylesheet.
	wp_enqueue_style( 'saintly-style', get_stylesheet_uri() );
    	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'saintly-compiled', get_template_directory_uri() . '/css/style.css');

    
	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'saintly-ie', get_template_directory_uri() . '/css/ie.css', array( 'saintly-style' ), '20141010' );
	wp_style_add_data( 'saintly-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'saintly-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'saintly-style' ), '20141010' );
	wp_style_add_data( 'saintly-ie7', 'conditional', 'lt IE 8' );


}
add_action( 'wp_enqueue_scripts', 'saintly_scripts' );


/**
 * Register new widgets here.
 */
function saintly_widgets_init() {
    register_sidebar( array(
		'name' => __( 'Saintly Contact Social', 'saintly' ),
		'id' => 'saintly-contact-social',
		'description' => __( 'Social Icon Area for Contact Us', 'saintly' ),
	) );
}
add_action( 'widgets_init', 'saintly_widgets_init' );


add_action( 'init', 'create_post_type' );

function create_post_type() {
  register_post_type( 'SaintlyClients',
    array(
      'labels' => array(
        'name' => __( 'Clients' ),
        'singular_name' => __( 'Client' ),
        ),
        'capability_type' =>  'post',
        'public' => true,
        'supports'	=>	array(
            'title',
            'editor',
            'revisions',
            'thumbnail',
            )
        )
  );

}


add_action( 'pre_get_posts', 'add_my_post_types_to_query' );

function add_my_post_types_to_query( $query ) {
  if ( is_home() && $query->is_main_query() )
    $query->set( 'post_type', array( 'post', 'page', 'SaintlyClients' ) );
  return $query;
}





