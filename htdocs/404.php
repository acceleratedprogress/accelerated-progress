<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Accelerate_Progress
 * @since Accelerate_Progress 1.0
 */

get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        	<div class="content-area">
                <section class="error-404 not-found">
                    <header class="page-header">
                        <h2 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'Accelerate_Progress' ); ?></h2>
                    </header><!-- .page-header -->

                    <div class="error-404 page-content">
                        <p><?php _e( 'It looks like nothing was found at this location. Better just stick to the <a href="http://deanmiranda.com">homepage</a>!', 'Accelerate_Progress' ); ?></p>

                    </div><!-- .page-content -->

                    <div id="contact">
                        <div class="contact-content">
                            <div id="social-icons">
                                <?php get_template_part('saintly-contact-social'); ?>
                            </div>
                        </div>
                    </div><!-- .contact-area -->
                </section><!-- .error-404 -->
            </div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>