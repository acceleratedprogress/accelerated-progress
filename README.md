# Accelerated Progress#

This is a custom built Wordpress theme for my web portfolio. Feel free to use it as is or as a base wordpress template to develop from.

### How do I get set up? ###

* You'll need wordpress installed.
* Clone or download this repo and save it as a zipped folder.
* Open your Wordpress site and deploy the zipped folder as your new theme.
* You'll want to install Grunt locally for any changes to the SCSS/JS files.

Questions?

* Reach me at deanmiranda.com
* Or email me directly at: dean.m.miranda@gmail.com