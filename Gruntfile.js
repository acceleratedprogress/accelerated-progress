module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'css/style.css': 'scss/style.scss'
                   
                } 
            }
        },
        cssmin: {
          options: {
            shorthandCompacting: false,
            roundingPrecision: -1
          },
          target: {
            files: {
              'css/style.min.css': ['css/style.css']
            }
          }
        },
        uglify: {
            my_target: {
              files: {
                'js/output.min.js': ['js/saintly.js']
              }
            }
        },
        watch: {
          grunt: { files: ['Gruntfile.js'] },

          sass: {
            files: 'scss/**/*.scss',
            tasks: ['sass'],
                  
          } 
        }
    });

      grunt.loadNpmTasks('grunt-contrib-sass');
      grunt.loadNpmTasks('grunt-contrib-cssmin');
      grunt.loadNpmTasks('grunt-contrib-uglify');
      grunt.loadNpmTasks('grunt-contrib-watch');

    
      grunt.registerTask('build', ['sass','cssmin','uglify']);
      grunt.registerTask('default', ['watch']);
}
